#ifndef _BUILDPILE_H
#define _BUILDPILE_H

#include "Pile.h"
#include "FaceUpPile.h"
#include "Card.h"

class BuildPile : public FaceUpPile
{
 public:
  BuildPile() : FaceUpPile() {}
  void addCard(const Card& c) override;
};

#endif
