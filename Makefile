CPPFLAGS = -std=c++11 -Wall -Wextra -pedantic -g
PP = g++

#Links together files needed to create skipbo executable
skipbo: main.o
	$(PP) -o skipbo main.o SkipBoGame.o Player.o BuildPile.o Hand.o PlayPile.o FaceUpPile.o DrawPile.o Pile.o Card.o

#Compiles Card.cpp to create Card.o
Card.o: Card.cpp Card.h
	$(PP) $(CPPFLAGS) -c Card.cpp

#Compiles Pile.cpp to create Pile.o
Pile.o: Pile.cpp Pile.h Card.o
	$(PP) $(CPPFLAGS) -c Pile.cpp

#Compiles DrawPile.cpp to create DrawPile.o
DrawPile.o: DrawPile.cpp DrawPile.h Pile.o
	$(PP) $(CPPFLAGS) -c DrawPile.cpp

#Compiles FaceUpPile.cpp to create FaceUpPile.o
FaceUpPile.o: FaceUpPile.cpp FaceUpPile.h Pile.o
	$(PP) $(CPPFLAGS) -c FaceUpPile.cpp

#Compiles PlayPile.cpp to create PlayPile.o
PlayPile.o: PlayPile.cpp PlayPile.h FaceUpPile.o
	$(PP) $(CPPFLAGS) -c PlayPile.cpp

#Compiles Hand.cpp to create Hand.o
Hand.o: Hand.cpp Hand.h PlayPile.o
	$(PP) $(CPPFLAGS) -c Hand.cpp

#Compiles BuildPile.cpp to create BuildPile.o
BuildPile.o: BuildPile.cpp BuildPile.h PlayPile.o
	$(PP) $(CPPFLAGS) -c BuildPile.cpp

#Compiles Player.cpp to create Player.o
Player.o:  Player.cpp Player.h Hand.o PlayPile.o
	$(PP) $(CPPFLAGS) -c Player.cpp

#Compiles SkipBoGame.cpp to create SkipBoGame.o
SkipBoGame.o: SkipBoGame.cpp SkipBoGame.h Player.o DrawPile.o BuildPile.o
	$(PP) $(CPPFLAGS) -c SkipBoGame.cpp

#Compiles main.cpp to create main.o
main.o: main.cpp SkipBoGame.o
	$(PP) $(CPPFLAGS) -c main.cpp

#Removes all object files and the executable
clean:
	rm -f *.o skipbo







