#ifndef _PLAYPILE_H
#define _PLAYPILE_H

#include "Pile.h"
#include "FaceUpPile.h"
#include "Card.h"



class PlayPile : public FaceUpPile {
public:
  PlayPile() : FaceUpPile() { }
  Card play(); //returns and removes top card
};

#endif
