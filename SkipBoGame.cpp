#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include "SkipBoGame.h"
#include "Player.h"


/* for live game play - must not change format!

drawPile  build_a  build_b  build_c  build_d
playerName  stock_0  
discards: discard_1 discard_2 discard_3 discard_4
hand: card_5 card_6 card_7 card_8 card_9
 */

//constructor for new game
SkipBoGame::SkipBoGame(std::string shuffle, int numPlayers, int stockSize, std::istream & is) 
{
  nump = numPlayers;
  //make players
  for(int i = 0; i < nump; i++) 
    peep.push_back(Player(i));
  //when game is shuffled
  if (shuffle == "true") {
    draw = DrawPile(is, 1);
    draw.shuffle();
    srand(time(0));
    curp = rand()%nump;
    shuffleMode = 1;
  }
  //when game is not shuffled
  else {
    draw = DrawPile(is, 0);
    curp = 0;
    shuffleMode = 0;
  }
  dealStockPiles(stockSize);
}

//constructor for game from save file
SkipBoGame::SkipBoGame(std::string shuffle, std::istream & is) {
  if (shuffle == "true")
    shuffleMode = 1;
  else 
    shuffleMode = 0;
  readIn(is);
  if (shuffleMode) {
    draw.setRand(1);
  }
  else
    draw.setRand(0);
}

//takes stock size and takes that many cards from the draw pile to make the stock pile, returns the stock pile
void SkipBoGame::dealStockPiles(int stockSize) {
  for (int i=0; i<stockSize; i++) {
    for (int i=0; i<nump; i++)
      peep.at((curp+i)%nump).stock.addCard(draw.drawCard());
  }
}

std::string SkipBoGame::getCurpName() const {
  return peep[curp].name;
}

void SkipBoGame::play() {
  //Display who's turn it is
  std::cout << std::endl;
  std::cout << " >> " << peep[curp].name << " turn next" << std::endl;
  //loops while players continue to choose to play
  char c = choice();
  while (c == 'p') {
    //turn will carry out curp's entire turn
    //breaks out of the loop if the game is over (returns 1)
    if (turn()){
      std::cout << "GAME OVER - " << peep[curp].name << " wins!" << std::endl;
      break;
    }
    //advances curp
    display();
    nextPlayer();
    //Displays next player's turn
    std::cout << std::endl;
    std::cout << " >> " << peep[curp].name << " turn next" << std::endl;
    c = choice();
  }
  //when player chooses to save
  if (c == 's') {
    std::cout << "save filename: ";
    std::string fname;
    std::cin >> fname;
    std::ofstream outfile;
    outfile.open(fname, std::ios::out);
    draw.shuffleIn();
    if (outfile.is_open()) {
      outfile << toString();
    }
  }
  //when player chooses to quit
  else if (c == 'q') {
    std::cout << "thanks for playing" << std::endl;
  }
}

//helper function that prompts whether the user wants to play, save, or quit
//returns the player's choice of those 3
char SkipBoGame::choice() {
  bool done = 0;
  char inp;
  while (!done) {
    //prompts player for input to play, save, or quit
    std::cout << "(p)lay, (s)ave, or (q)uit ? ";
    std::cin >> inp;
    //check to see if the user provided a valid command
    if (inp != 'p' && inp != 's' && inp != 'q') {
      std::cout << "illegal command, try again" << std::endl;
      std::cout << std::endl;
      std::cout << " >> " << peep[curp].name << " turn next" << std::endl;
    }
    else
      done=1;
  }
  return inp;
}
  
//takes the characters representing the start and end piles for playing a card; returns whether or not the turns is over (card moves to a discard pile)
bool SkipBoGame::move(char start, char end)
{
  //notes: buildpile class should have its own addCard method which checks if card being added is a valid play and then calls the Pile's addCard method
  bool turnOver = false;
  bool inval = false; //move to build failed
  if(start == '0') //move from stock
    {
      if(end >= 'a' && end <= 'd') //move from stock to build
	{
	  //add player.stock.top to build
	  int i = 3 - ('d'-end); //index of target buildpile
	  try {
	    build[i].addCard(peep[curp].play('s', 0));
	  }
	  catch (std::exception& e) {
	    inval = true;
	  }
	  if (!inval)
	    peep[curp].stock.removeCard();
	  //set aside completed build pile
	  if (build[i].getSize() == 12)
	    setAsideBuild(i);
	}
      else
	  inval = true;
    }
  else if(start >= '1' && start <= '4') //move from discard
    {
      if(end >= 'a' && end <= 'd') //move from discard to build
	{
	  int i = 3 - ('d'-end); //index of target buildpile
	  int j = 3 - ('4'-start); //index of target discard
	  try {
	    build[i].addCard(peep[curp].play('d', j));
	  }
	  catch (std::exception& e) {
	    inval = true;
	  }
	  if (!inval)
	    peep[curp].discard[j].removeCard();
	  //set aside completed build pile
	  if (build[i].getSize() == 12)
	    setAsideBuild(i);
	}
      else
        inval = true;
    }
  else if(start >= '5' && start <= '9') //move from hand
    {
      int j = 4 - ('9'-start); //index of target hand
      if (j >= peep[curp].hand.getSize())
        inval = true;
      else {
	if(end >= 'a' && end <= 'd') //move from hand to build
	  {
	    int i = 3 - ('d'-end); //index of target buildpile
	    try {
	      build[i].addCard(peep[curp].play('h', j));
	    }
	    catch (std::exception& e) {
	      inval = true;
	    }
	    if (!inval)
	      peep[curp].hand.removeCard(j);
	    //set aside completed build pile
	    if (build[i].getSize() == 12)
	      setAsideBuild(i);
	  }
	else if(end >= '1' && end <= '4') //move from hand to discard
	  {
	    int i =  3 - ('4'-end); //index of target discard
	    peep[curp].discard[i].addCard(peep[curp].play('h', j));
	    peep[curp].hand.removeCard(j);
	    turnOver = true; //curp's turn is over
	  }
	else
	  inval = true;
      }
    }
  else
    inval=true;
  if(inval) //move was invalid
    {
      //print error, invalid move
      std::cout << "illegal command, try again" << std::endl;
    }  
  return turnOver;
}

//takes the cards from the ith build pile and sets them aside in a vector in drawpile
void SkipBoGame::setAsideBuild(int i) {
  int size = build[i].size();
  for (int j=0; j<size; j++)
    draw.setAside(build[i].pop(0));
  std::cout << "build pile " << (char)('a' + i) << " full, set aside" << std::endl;
}

//makes all of the specified moves during curp's turn; boolean return true if the game is over, false if not
bool SkipBoGame::turn() {
  bool turnOver = false;
  bool gameOver = false;
  fillHand();
  do {
    //display current game state
    display();
    //take user input
    char inp = prompt();
    //move card choice
    if (inp == 'm') {
      //input information about the specific move
      char start, end;
      if (std::cin >> start >> end) {
	//helper function to move the card
	turnOver = move(start, end); //makes move and checks if turn over
      }
      else {
	std::cout << "illegal command, try again" << std::endl;
      }
    }
    //draw card choice
    else if (inp == 'd') {
      //when curp's hand is not empty
      if (!peep[curp].hand.isEmpty())
	std::cout << "illegal command, try again" << std::endl;
      //curp's hand is empty
      else {
	fillHand();
      }
    }
    std::cout << std::endl;
    //checks if game is over
    gameOver = peep[curp].stock.isEmpty();
    if (gameOver)
      turnOver = 1;
  } while(!turnOver);
  return gameOver;
}

//fills curp's hand to have 5 cards
void SkipBoGame::fillHand() {
  for(int i = peep[curp].getHandSize(); i < 5; i++)
	peep[curp].addToHand(draw.drawCard());	       
}

//Makes the next player curp
void SkipBoGame::nextPlayer() {
  curp = (curp + 1) % nump;
}

void SkipBoGame::display() const {
  std::cout << "Draw: ";
  draw.display();
  std::cout << "  Build Piles: ";
  for (int j = 0; j < 4; j++) {
    build[j].display();
    std::cout << " ";
  }
  std::cout << std::endl;
  peep[curp].display();
}

/* for saving state - must not change format!

shuffle numplayers currplayer
PlayerCurp [display]
PlayerCurp+1 [display]
[etc for all players]
Draw [display]
Build_a [display]
Build_b [display]
Build_c [display]
Build_d [display]
*/
std::string SkipBoGame::toString() const {
  std::stringstream result;
  int idx;
  result << draw.getRand() << " " << nump << " " << curp << "\n";
  for (int i = 0; i < nump; ++i) {
    idx = (curp+i) % nump;
    result << peep[idx].toString();
  }
  result << "Draw " << draw.toString(); 
  for (int j = 0; j < 4; j++) {
    result << "Build_" << char('a'+j) << " ";
    result << build[j].toString();  
  }
  return result.str();
}

void SkipBoGame::readIn(std::istream& is) {
  //extracting info about the saved game
  std::string wasRand;
  is >> wasRand >> nump >> curp;
  //loop to initialize the players
  for (int i=0; i<nump; i++) {
    peep.push_back(Player(i));
  }
  //loop through each player to read their info provided in the istream
  for (int i=0; i<nump; i++) {
    peep[(curp+i)%nump].readIn(is);
  }
  //extracting info about the draw pile
  std::string pileName;
  int pileSize;
  is >> pileName >> pileSize;
  //builds draw pile if size > 0
  if (pileSize)
    draw.readIn(is, pileSize);
  if (wasRand == "false" && shuffleMode == 1)
    draw.shuffle();
  //loop for each of the build piles
  for (int i=0; i<4; i++) {
    //etracting build pile info
    is >> pileName >> pileSize;
    //builds builkd pile if size > 0
    if (pileSize)
      build[i].readIn(is, pileSize);
  }
}

//prompts user for input and returns their choice
char SkipBoGame::prompt() {
  std::string inp;
  bool done = 0;
  while (!done) {
    //prompt user for input to move or draw
    std::cout << "(m)ove [start] [end] or (d)raw ? ";
    //input user's chosen move
    std::cin >> inp;
    //check to see if the user provided a valid command
    if (inp.size() != 1 || (inp[0] != 'm' && inp[0] != 'd'))
      std::cout << "illegal command, try again" << std::endl;
    else
      done=1;
  }
  return inp[0];
}
