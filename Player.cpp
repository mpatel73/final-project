#include <iostream>
#include <string>
#include <sstream>
#include "Player.h"

Player::Player(int pnum) {
  //making the name corresponding to the player number
  name = "Player" + std::to_string(pnum);
  stock = PlayPile();
  hand = Hand();
  for (int i=0; i<4; i++) {
    discard[i] = PlayPile();
  }
}
// for live game play - must not change!
void Player::display() const {
  std::cout << "Current ";
  std::cout << name << ":  Stock{0}: " ;
  stock.display();
  std::cout << std::endl;
  std::cout << "   Discards{1-4}: ";
  for (int i = 0; i < 4; ++i) {
    discard[i].display();
    std::cout << "  ";
  }
  std::cout << std::endl;
  std::cout << "   Hand{5-9}: ";
  hand.display();
  std::cout << std::endl;
}

/* saving state format - must not change!
PlayerName
Stock size
01 02 03  ...
...
Hand size
01 02 03 04 05
Discard1 size
01 02 03 04 ...
...
Discard2 size
...
Discard3 size
...
Discard4 size
...
*/
std::string Player::toString() const {
  std::stringstream result;
  result << name << "\n";
  result << "Stock " << stock.toString();
  result << "Hand " << hand.toString();
  for (int i = 0; i < 4; ++i) {
    result << "Discard" << i << " " << discard[i].toString();
  }
  return result.str();
}

void Player::readIn(std::istream& is) {
  //making the player's name the specificed name
  is >> name;
  //varaibles to store the name of the pile and its size
  std::string pileName;
  int pileSize;
  //extracting stock pile info
  is >> pileName >> pileSize;
  //builds stock pile if its size > 0
  if (pileSize)
    stock.readIn(is, pileSize);
  //extracting hand pile info
  is >> pileName >> pileSize;
  if (pileSize)
    hand.readIn(is, pileSize);
  //loop for each of the 4 discard piles
  for (int i=0; i<4; i++) {
    //extracting ith discard pile info
    is >> pileName >> pileSize;
    //builds discard pile if size > 0
    if (pileSize)
      discard[i].readIn(is, pileSize);
  }
}

Card Player::play(char t, int i)
{
  if(t == 's') //play from stock
    return(stock.play());
  else if(t == 'd') //play from discard
    return(discard[i].play());
  else //if h; play from hand
    return(hand.play(i));
	
}

int Player::getHandSize() {
  return hand.getSize();
}

void Player::addToHand(Card c)
{
	hand.addCard(c);
}

void Player::addToDiscard(Card c, int i)
{
	discard[i].addCard(c);
}

//check if stock empty
bool Player::stockEmpty()
{
	if(stock.size() == 0)
		return true;
	else
		return false;
}
