#ifndef _SKIPBOGAME_H
#define _SKIPBOGAME_H

#include "Player.h"
#include "DrawPile.h"
#include "BuildPile.h"
#include "PlayPile.h"
#include <vector>
#include <string>
#include <iostream>

class SkipBoGame {
 public:
  SkipBoGame(std::string shuffle, int numPlayers, int stockSize, std::istream & is);
  SkipBoGame(std::string shuffle, std::istream & is);
  void dealStockPiles(int stockSize);
  std::string getCurpName() const;
  bool turn();
  void setAsideBuild(int i);
  void play();
  bool move(char start, char end);
  void fillHand();
  void nextPlayer();
  void display() const;
  std::string toString() const;
  void readIn(std::istream& is);
  
 private:
  DrawPile draw;
  BuildPile build[4];
  std::vector<Player> peep;
  int nump;
  int curp;
  char prompt();
  char choice();
  bool shuffleMode;
};

#endif
