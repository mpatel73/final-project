#include "SkipBoGame.h"
#include <iostream>
#include <fstream>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;

char prompt();

int main (int argc, char* args[]) {
  if (argc == 1) {
    cerr << "invalid program usage: invalid number or arguments" << endl;
    return 1;
  }
  std::string isShuffle(args[1]); //converts 1st argument to a string
  //GAME SET UP
  //New Game
  if (argc == 5) {
    //Boolean Not Provided
    if (isShuffle != "false" && isShuffle != "true") {
      cout << "invalid program usage: invalid first argument" << endl;
      return 1;
    }
    int nplayers = std::stoi(args[2]); //convert the 2nd argument to an integer to check number of players
    //Too many or too few players
    if (nplayers > 6 || nplayers < 2) {
      cout << "invalid program usage: num players must be 2-6" << endl;
      return 1;
    }
    //Outputs number of players if valid
    cout << "num players is " << nplayers << endl;
    //Stock size that does not make sense
    int ssize = std::stoi(args[3]); //convert the 3rd argument to an integer to check stock size
    if (ssize <= 0 || ssize > 30 || (nplayers == 6 && ssize > 20)) {
      cout << "invalid program usage: bad stock size" << endl;
      return 1;
    }
    //Outputs stock size if valid
    cout << "stock size is " << ssize << endl;
    //Creates and opens deck file
    ifstream deckFile;
    deckFile.open(args[4], ifstream::in);
    //deckFile fails to open
    if (!deckFile.is_open()) {
      cout << "invalid program usage: can't open deck file" << endl;
      return 1;
    }
    //create game
    SkipBoGame game(isShuffle, nplayers, ssize, deckFile);
    game.play();
  }
  //Saved Game
  else if (argc == 3) {
    //Boolean Not Provided
    if (isShuffle != "false" && isShuffle != "true") {
      cout << "invalid program usage: invalid first argument" << endl;
      return 1;
    }
    //Creates and opens game file
    ifstream gameFile;
    gameFile.open(args[2], ifstream::in);
    //game file fails to open
    if (!gameFile.is_open()) {
      cout << "invalid program usage: can't open input game file" << endl;
      return 1;
    }
    //create game
    SkipBoGame game(isShuffle, gameFile);
    game.play();
  }
  //Incorrect number of arguments
  else {
    cout << "invalid program usage: invalid number of arguments" << endl;
    return 1;
  }
  return 0;
}

//helper function that prompts whether the user wants to play, save, or quit
//returns the player's option of those 3
char prompt() {
  bool done = 0;
  char inp;
  while (!done) {
    //prompts player for input to play, save, or quit;
    cout << "(p)lay, (s)ave, (q)uit ? ";
    cin >> inp;
    //check to see if the user provided a valid command
    if (inp != 'p' && inp != 's' && inp != 'q')
      cout << "illegal command, try again" << endl;
    else
      done=1;
  }
  return inp;
}
