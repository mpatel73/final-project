#ifndef _PLAYER_H
#define _PLAYER_H
#include "BuildPile.h"
#include "PlayPile.h"
#include "Hand.h"
#include <vector>
#include <string>

class Player {
public:
	Player(int pnum);
  	void display() const;
  	std::string toString() const;
  	void readIn(std::istream& is);
  	bool stockEmpty();
  	Card play(char t, int i);
  	int getHandSize();
  	void addToHand(Card c);
  	void addToDiscard(Card c, int i);
  	friend class SkipBoGame;
  
private:
	std::string name;
  	Hand hand;
  	PlayPile stock;
  	PlayPile discard[4];
};

#endif
