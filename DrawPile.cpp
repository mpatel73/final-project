#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <time.h>
#include "DrawPile.h"

//Constructor for DrawPile
DrawPile::DrawPile(std::istream & is, bool r) {
  readIn(is);
  rand=r;
}

//default constructor
DrawPile::DrawPile() : Pile()
{
  rand = 0;
}

//takes a draw pile and removes the top card from the pile; returns the card
Card DrawPile::drawCard() {
  if (pile.size() == 0)
    shuffleIn();
  return pop();
}

//shuffles cards in the pile
void DrawPile::shuffle() {
  srand(time(0));
  std::random_shuffle(pile.begin(), pile.end());
}

//accessor for rand
std::string DrawPile::getRand() const {
  if (rand)
    return "true";
  return "false";
}

//mutator for rand
void DrawPile::setRand(bool b) {
  rand=b;
}

// for live game play - must not change!
void DrawPile::display() const {
  std::cout << "[XX]/" << size();
}

void DrawPile::setAside(Card c) {
  aside.push_back(c);
}

void DrawPile::shuffleIn() {
  int size = aside.size();
  pile.insert(pile.begin(), aside.begin(), aside.end());
  for (int i=0; i<size; i++) {
    aside.pop_back();
  }
  if (rand)
    shuffle();
}
