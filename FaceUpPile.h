#ifndef _FACEUPPILE_H
#define _FACEUPPILE_H

#include "Pile.h"
#include "Card.h"

class FaceUpPile : public Pile {
 public:
  FaceUpPile() : Pile() { }
  void display() const;
};

#endif
