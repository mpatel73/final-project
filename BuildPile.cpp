#include "BuildPile.h"

//checks if c is top+1, then calls pile's addCard
void BuildPile::addCard(const Card& c)
{
  if((short)pile.size()+1 == c.getValue() || c.getValue() == 0) //c is a valid play
    Pile::addCard(c); //calls Pile's addCard
  else
    throw (std::overflow_error("error"));	
}
