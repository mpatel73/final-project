#ifndef _HAND_H
#define _HAND_H

#include "Pile.h"
#include "FaceUpPile.h"
#include "PlayPile.h"
#include "Card.h"

class Hand : public PlayPile
{
public:
  Hand() : PlayPile() { }
  void display() const;
  Card play(int i);
  void removeCard(int i);
  using Pile::removeCard;
};

#endif
