#ifndef _PILE_H
#define _PILE_H

#include <vector>
#include <iostream>
#include "Card.h"

class Pile {
 protected:
  std::vector<Card> pile;

 public:
  Pile() { }

  int size() const { return pile.size(); }

  virtual void addCard(const Card& c) { pile.push_back(c); }

  virtual void removeCard() { pile.pop_back(); }

  Card pop() { Card c = pile.back(); removeCard(); return c; }
  
  Card pop(int i) { Card c = pile.at(i); pile.erase(pile.begin() + i); return c; }
  
  std::string toString() const;  // for saving state

  void readIn(std::istream & is);  // students to write this, companion to toString()

  void readIn(std::istream & is, int pileSize);

  virtual void display() const = 0;  // for live game play, must override

  bool isEmpty() { return pile.size() == 0; }

  int getSize() { return pile.size(); }
};


#endif
