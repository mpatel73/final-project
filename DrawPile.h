#ifndef _DRAWPILE_H
#define _DRAWPILE_H
#include <iostream>
#include <vector>
#include <string>
#include "Pile.h"
#include "Card.h"

class DrawPile : public Pile {	
public:
	DrawPile(std::istream & is, bool rand);
	DrawPile();
	Card drawCard();
	void shuffle();
	std::string getRand() const;
	void setRand(bool b);
	void display() const;
	void setAside(Card c);
	void shuffleIn();
private:
	bool rand;
	std::vector<Card> aside;
};

#endif
