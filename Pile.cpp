#include <vector>
#include <iostream>
#include "Pile.h"

using std::istream;

// for saving game state - must not change!
std::string Pile::toString() const {
  std::string result;
  result.append(std::to_string(size()));
  result.append("\n");
  // add all the pile elements to the string, at most 20 per line
  for (int i=0; i < size(); ++i) {
    if (i % 20 == 0 && i != 0)
      result.append("\n");
    else if (i != 0)
      result.append(" ");
    result.append(pile[i].toString());
  }
  result.append("\n");
  return result;
}

//building a pile from a given istream of unspecified size (ie deck files)
void Pile::readIn(istream & is) {
  int num;
  //loop through each piece of the input file
  while (is >> num) {
    //Adds card with that value to the pile
    addCard(Card(num));
  }
}

//building a pile from a given istream of specified size (ie save files)
void Pile::readIn(istream & is, int pileSize) {
  int num;
  //loop through to obtain the specified number of cards
  for (int i=0; i<pileSize; i++) {
    //Adds card with that value to the pile
    is >> num;
    Card c = Card(num);
    addCard(c);
  }
}

